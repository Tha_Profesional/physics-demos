/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef ENTITY_H_INCLUDED
#define ENTITY_H_INCLUDED

#include "globals.h"

struct Entity
{
    unsigned Id;

    Entity() {}

    Entity(int i) {
        Id = i;
    }

    unsigned Index() const {
        return Id & Globals::ENTITY_INDEX_MASK;
    }

    unsigned Generation() const {
        return (Id >> Globals::ENTITY_INDEX_BITS) & Globals::ENTITY_GENERATION_MASK;
    }
};

#endif // ENTITY_H_INCLUDED
