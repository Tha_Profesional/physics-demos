/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef PHYSICSCOMPONENTCOLLECTION_H_INCLUDED
#define PHYSICSCOMPONENTCOLLECTION_H_INCLUDED

#include <vector>

#include "entity.h"
#include "vector.h"
#include "shape.h"

struct Entity;

struct PhysicsComponentCollection {
    std::vector<struct Entity> Entity;
    std::vector<float> InverseMass;
    std::vector<float> Friction;
    std::vector<float> Elasticity;
    std::vector<class Shape*> Shape;
};

#endif // PHYSICSCOMPONENTCOLLECTION_H_INCLUDED
