/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef MOVEMENTCOMPONENTCOLLECTION_H_INCLUDED
#define MOVEMENTCOMPONENTCOLLECTION_H_INCLUDED

#include <vector>

#include "entity.h"
#include "vector.h"

struct Entity;

struct MovementComponentCollection {
    std::vector<struct Entity> Entity;
    std::vector<Vector> Velocity;
};

#endif // MOVEMENTCOMPONENTCOLLECTION_H_INCLUDED
