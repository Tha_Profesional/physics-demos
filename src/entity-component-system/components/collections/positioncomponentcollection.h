/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef POSITIONCOMPONENTCOLLECTION_H_INCLUDED
#define POSITIONCOMPONENTCOLLECTION_H_INCLUDED

#include <vector>

#include "entity.h"
#include "vector.h"

struct Entity;

struct PositionComponentCollection {
    std::vector<struct Entity> Entity;
    std::vector<Vector> Position;
    std::vector<float> Angle;
};

#endif // POSITIONCOMPONENTCOLLECTION_H_INCLUDED
