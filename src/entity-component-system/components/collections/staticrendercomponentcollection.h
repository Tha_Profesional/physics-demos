/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef STATICRENDERCOMPONENTCOLLECTION_H_INCLUDED
#define STATICRENDERCOMPONENTCOLLECTION_H_INCLUDED

#include <vector>

#include "SDL2/SDL.h"

#include "entity.h"
#include "textureid.h"
#include "vector.h"

struct Entity;

struct StaticRenderComponentCollection {
    std::vector<struct Entity> Entity;
    std::vector<struct TextureId> TextureId;
    std::vector<SDL_Rect> Clip;
    std::vector<struct Vector> Position;
};

#endif // STATICRENDERCOMPONENTCOLLECTION_H_INCLUDED

