/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef POSITIONCOMPONENTMANAGER_H_INCLUDED
#define POSITIONCOMPONENTMANAGER_H_INCLUDED

#include <unordered_map>

#include "positioncomponentcollection.h"
#include "entity.h"
#include "componentid.h"
#include "vector.h"

class PositionComponentManager {
private:
    PositionComponentCollection collection;

    std::unordered_map<unsigned, unsigned> map;
public:
    ComponentId Create(Entity e);
    void Destroy(ComponentId i);

    // TODO: Do I need this? It's nice - but not really how I roll.
    //void GarbageCollect(const EntityManager& em);

    int GetCount();

    ComponentId GetId(Entity e);

    Entity GetEntity(ComponentId i);

    Vector GetPosition(ComponentId i);
    void SetPosition(ComponentId i, Vector p);

    float GetAngle(ComponentId i);
    void SetAngle(ComponentId i, float a);
};

#endif // POSITIONCOMPONENTMANAGER_H_INCLUDED
