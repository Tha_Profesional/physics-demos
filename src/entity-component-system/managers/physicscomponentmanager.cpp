/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "physicscomponentmanager.h"

#include "circle.h"
#include "rectangle.h"

using namespace std;

ComponentId PhysicsComponentManager::Create(Entity e) {
    if (map.find(e.Id) != map.end())
        return { -1 };

    collection.Entity.push_back(e);
    collection.InverseMass.push_back(0.0);
    collection.Friction.push_back(0.0);
    collection.Elasticity.push_back(0.0);
    collection.Shape.push_back(nullptr);

    map[e.Id] = collection.Entity.size() - 1;

    return { static_cast<int>(map[e.Id]) };
}

void PhysicsComponentManager::Destroy(ComponentId i) {
    int lastIndex = collection.Entity.size() - 1;
    Entity entity = collection.Entity[i.Id];

    delete collection.Shape[i.Id];

    collection.Entity[i.Id] = collection.Entity[lastIndex];
    collection.InverseMass[i.Id] = collection.InverseMass[lastIndex];
    collection.Friction[i.Id] = collection.Friction[lastIndex];
    collection.Elasticity[i.Id] = collection.Elasticity[lastIndex];
    collection.Shape[i.Id] = collection.Shape[lastIndex];

    collection.Entity.erase(collection.Entity.begin() + lastIndex);
    collection.InverseMass.erase(collection.InverseMass.begin() + lastIndex);
    collection.Friction.erase(collection.Friction.begin() + lastIndex);
    collection.Elasticity.erase(collection.Elasticity.begin() + lastIndex);
    collection.Shape.erase(collection.Shape.begin() + lastIndex);

    map[collection.Entity[i.Id].Id] = i.Id;
    map.erase(map.find(entity.Id));
}

ComponentId PhysicsComponentManager::GetId(Entity e) {
    if (map.find(e.Id) == map.end())
        return { -1 };

    return { static_cast<int>(map[e.Id]) };
}

int PhysicsComponentManager::GetCount() {
    return collection.Entity.size();
}

Entity PhysicsComponentManager::GetEntity(ComponentId i) {
    return collection.Entity[i.Id];
}

float PhysicsComponentManager::GetInverseMass(ComponentId i) {
    return collection.InverseMass[i.Id];
}

void PhysicsComponentManager::SetInverseMass(ComponentId i, float im) {
    collection.InverseMass[i.Id] = im;
}

float PhysicsComponentManager::GetFriction(ComponentId i) {
    return collection.Friction[i.Id];
}

void PhysicsComponentManager::SetFriction(ComponentId i, float f) {
    collection.Friction[i.Id] = f;
}

float PhysicsComponentManager::GetElasticity(ComponentId i) {
    return collection.Elasticity[i.Id];
}

void PhysicsComponentManager::SetElasticity(ComponentId i, float e) {
    collection.Elasticity[i.Id] = e;
}

template <typename T>
T PhysicsComponentManager::GetShape(ComponentId i) {
    const T* shape = static_cast<T*>(collection.Shape[i.Id]);

    return *shape;
}

template Circle PhysicsComponentManager::GetShape<Circle>(ComponentId);
template Rectangle PhysicsComponentManager::GetShape<Rectangle>(ComponentId);
template Shape PhysicsComponentManager::GetShape<Shape>(ComponentId);

template <typename T>
void PhysicsComponentManager::SetShape(ComponentId i, T s) {
    if (collection.Shape[i.Id] == nullptr)
        collection.Shape[i.Id] = new T();
        
    *(collection.Shape[i.Id]) = s;
}

template void PhysicsComponentManager::SetShape<Circle>(ComponentId, Circle);
template void PhysicsComponentManager::SetShape<Rectangle>(ComponentId, Rectangle);
template void PhysicsComponentManager::SetShape<Shape>(ComponentId, Shape);
