/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef PHYSICSCOMPONENTMANAGER_H_INCLUDED
#define PHYSICSCOMPONENTMANAGER_H_INCLUDED

#include <unordered_map>

#include "physicscomponentcollection.h"
#include "componentid.h"
#include "shape.h"

class PhysicsComponentManager {
private:
    PhysicsComponentCollection collection;

    std::unordered_map<unsigned, unsigned> map;
public:
    ComponentId Create(Entity e);
    void Destroy(ComponentId i);

    ComponentId GetId(Entity e);

    int GetCount();

    Entity GetEntity(ComponentId i);

    float GetInverseMass(ComponentId i);
    void SetInverseMass(ComponentId i, float im);

    float GetFriction(ComponentId i);
    void SetFriction(ComponentId i, float f);

    float GetElasticity(ComponentId i);
    void SetElasticity(ComponentId i, float e);

    template <typename T>
    T GetShape(ComponentId i);
    template <typename T>
    void SetShape(ComponentId i, T s);
};

#endif // PHYSICSCOMPONENTMANAGER_H_INCLUDED
