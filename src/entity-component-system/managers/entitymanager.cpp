/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "entitymanager.h"

#include <stdexcept>

using namespace std;

Entity EntityManager::Create() {
    unsigned idx;

    if (_free_indices.size() > Globals::MINIMUM_FREE_INDICES) {
        idx = _free_indices.front();
        _free_indices.pop_front();
    } else {
        _generation.push_back(0);
        idx = _generation.size() - 1;

        if (idx > (1 << Globals::ENTITY_INDEX_BITS))
            throw runtime_error("Error: Too many entities have been created -" + (1 << Globals::ENTITY_INDEX_BITS)); // TODO: Fix error message
    }

    return { idx | _generation[idx] };
}

bool EntityManager::Alive(Entity e) const {
    return _generation[e.Index()] == e.Generation();
}

void EntityManager::Destroy(Entity e) {
    const unsigned idx = e.Index();
    ++_generation[idx];
    _free_indices.push_back(idx);
}
