/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef MOVEMENTCOMPONENTMANAGER_H_INCLUDED
#define MOVEMENTCOMPONENTMANAGER_H_INCLUDED

#include <unordered_map>

#include "movementcomponentcollection.h"
#include "entity.h"
#include "componentid.h"
#include "vector.h"

class MovementComponentManager {
private:
    MovementComponentCollection collection;

    std::unordered_map<unsigned, unsigned> map;
public:
    ComponentId Create(Entity e);
    void Destroy(ComponentId i);

    int GetCount();

    ComponentId GetId(Entity e);

    Entity GetEntity(ComponentId i);

    Vector GetVelocity(ComponentId i);
    void SetVelocity(ComponentId i, Vector v);
};

#endif // MOVEMENTCOMPONENTMANAGER_H_INCLUDED
