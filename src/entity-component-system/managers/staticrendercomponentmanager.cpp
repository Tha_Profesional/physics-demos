/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "staticrendercomponentmanager.h"

using namespace std;

ComponentId StaticRenderComponentManager::Create(Entity e) {
    if (map.find(e.Id) != map.end())
        return { -1 };

    collection.Entity.push_back(e);
    collection.TextureId.push_back(TextureId());
    collection.Clip.push_back(SDL_Rect());
    collection.Position.push_back(Vector());

    map[e.Id] = collection.Entity.size() - 1;

    return { map[e.Id] };
}

void StaticRenderComponentManager::Destroy(ComponentId i) {
    int lastIndex = collection.Entity.size() - 1;
    Entity entity = collection.Entity[i.Id];

    collection.Entity[i.Id] = collection.Entity[lastIndex];
    collection.TextureId[i.Id] = collection.TextureId[lastIndex];
    collection.Clip[i.Id] = collection.Clip[lastIndex];
    collection.Position[i.Id] = collection.Position[lastIndex];

    collection.Entity.erase(collection.Entity.begin() + lastIndex);
    collection.TextureId.erase(collection.TextureId.begin() + lastIndex);
    collection.Clip.erase(collection.Clip.begin() + lastIndex);
    collection.Position.erase(collection.Position.begin() + lastIndex);

    map[collection.Entity[i.Id].Id] = i.Id;
    map.erase(map.find(entity.Id));
}

int StaticRenderComponentManager::GetCount() {
    return collection.Entity.size();
}

ComponentId StaticRenderComponentManager::GetId(Entity e) {
    if (map.find(e.Id) == map.end())
        return { -1 };
    else
        return { map[e.Id] };
}

Entity StaticRenderComponentManager::GetEntity(ComponentId i) {
    return collection.Entity[i.Id];
}

TextureId StaticRenderComponentManager::GetTextureId(ComponentId i) {
    return collection.TextureId[i.Id];
}

void StaticRenderComponentManager::SetTextureId(ComponentId i, TextureId t) {
    collection.TextureId[i.Id] = t;
}

SDL_Rect StaticRenderComponentManager::GetClip(ComponentId i) {
    return collection.Clip[i.Id];
}

void StaticRenderComponentManager::SetClip(ComponentId i, SDL_Rect r) {
    collection.Clip[i.Id] = r;
}

Vector StaticRenderComponentManager::GetPosition(ComponentId i) {
    return collection.Position[i.Id];
}

void StaticRenderComponentManager::SetPosition(ComponentId i, Vector p) {
    collection.Position[i.Id] = p;
}

