/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef ENTITYMANAGER_H_INCLUDED
#define ENTITYMANAGER_H_INCLUDED

#include <vector>
#include <deque>

#include "entity.h"

class EntityManager {
private:
    std::vector<unsigned char> _generation;
    std::deque<unsigned> _free_indices;
public:
    Entity Create();

    bool Alive(Entity e) const;

    void Destroy(Entity e);
};

#endif // ENTITYMANAGER_H_INCLUDED
