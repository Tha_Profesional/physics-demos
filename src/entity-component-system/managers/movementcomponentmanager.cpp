/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "movementcomponentmanager.h"

using namespace std;

ComponentId MovementComponentManager::Create(Entity e) {
    if (map.find(e.Id) != map.end())
        return { -1 };

    collection.Entity.push_back(e);
    collection.Velocity.push_back({0, 0});

    map[e.Id] = collection.Entity.size() - 1;

    return { map[e.Id] };
}

void MovementComponentManager::Destroy(ComponentId i) {
    int lastIndex = collection.Entity.size() - 1;
    Entity entity = collection.Entity[i.Id];

    collection.Entity[i.Id] = collection.Entity[lastIndex];
    collection.Velocity[i.Id] = collection.Velocity[lastIndex];

    collection.Entity.erase(collection.Entity.begin() + lastIndex);
    collection.Velocity.erase(collection.Velocity.begin() + lastIndex);

    map[collection.Entity[i.Id].Id] = i.Id;
    map.erase(map.find(entity.Id));
}

int MovementComponentManager::GetCount() {
    return collection.Entity.size();
}

ComponentId MovementComponentManager::GetId(Entity e) {
    if (map.find(e.Id) == map.end())
        return { -1 };
    else
        return { map[e.Id] };
}

Entity MovementComponentManager::GetEntity(ComponentId i) {
    return collection.Entity[i.Id];
}

Vector MovementComponentManager::GetVelocity(ComponentId i) {
    return collection.Velocity[i.Id];
}

void MovementComponentManager::SetVelocity(ComponentId i, Vector v) {
    collection.Velocity[i.Id] = v;
}

