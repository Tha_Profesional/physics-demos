/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "positioncomponentmanager.h"

using namespace std;

ComponentId PositionComponentManager::Create(Entity e) {
    if (map.find(e.Id) != map.end())
        return { -1 };

    collection.Entity.push_back(e);
    collection.Position.push_back({0, 0});
    collection.Angle.push_back(0.0);

    map[e.Id] = collection.Entity.size() - 1;

    return { map[e.Id] };
}

void PositionComponentManager::Destroy(ComponentId i) {
    int lastIndex = collection.Entity.size() - 1;
    Entity entity = collection.Entity[i.Id];

    collection.Entity[i.Id] = collection.Entity[lastIndex];
    collection.Position[i.Id] = collection.Position[lastIndex];
    collection.Angle[i.Id] = collection.Angle[lastIndex];

    collection.Entity.erase(collection.Entity.end() + lastIndex);
    collection.Position.erase(collection.Position.end() + lastIndex);
    collection.Angle.erase(collection.Angle.end() + lastIndex);

    map[collection.Entity[i.Id].Id] = i.Id;
    map.erase(map.find(entity.Id));
}

/*void PositionComponentManager::GarbageCollect(const EntityManager& em) {
    unsigned aliveCount;

    while (collection.Entity.count() > 0 && aliveCount < 4) { // TODO: Get rid of magic number.
        unsigned i = 0; // TODO: Make random.
        // TODO: Use this maybe: http://stackoverflow.com/questions/7114043/random-number-generation-in-c11-how-to-generate-how-do-they-work

        if (em.Alive(collection.Entity[i])) {
            ++aliveCount;
        } else {
            aliveCount = 0;
            Destroy(i);
        }
    }
}*/

ComponentId PositionComponentManager::GetId(Entity e) {
    if (map.find(e.Id) == map.end())
        return { -1 };
    else
        return { map[e.Id] };
}

int PositionComponentManager::GetCount() {
    return collection.Entity.size();
}

Entity PositionComponentManager::GetEntity(ComponentId i) {
    return collection.Entity[i.Id];
}

Vector PositionComponentManager::GetPosition(ComponentId i) {
    return collection.Position[i.Id];
}

void PositionComponentManager::SetPosition(ComponentId i, Vector p) {
    collection.Position[i.Id] = p;
}

float PositionComponentManager::GetAngle(ComponentId i) {
    return collection.Angle[i.Id];
}
void PositionComponentManager::SetAngle(ComponentId i, float a) {
    collection.Angle[i.Id] = a;
}
