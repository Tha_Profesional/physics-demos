/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef RENDERERINGSYSTEM_H_INCLUDED
#define RENDERERINGSYSTEM_H_INCLUDED

#include "sdlrenderer.h"
#include "texturemanager.h"
#include "staticrendercomponentmanager.h"
#include "positioncomponentmanager.h"

class RenderingSystem {
private:
    SDLRenderer* renderer;

    TextureManager* textureManager;
public:
    RenderingSystem(SDLRenderer* r, TextureManager* t);

    void Render(StaticRenderComponentManager& s, PositionComponentManager& p);
};

#endif // RENDERERINGSYSTEM_H_INCLUDED

