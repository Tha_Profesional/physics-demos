/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "renderingsystem.h"

#include "SDL2/SDL.h"

#include "vector.h"

using namespace std;

RenderingSystem::RenderingSystem(SDLRenderer* r, TextureManager* t) : renderer(r), textureManager(t) {}

void RenderingSystem::Render(StaticRenderComponentManager& s, PositionComponentManager& p) {
    for (int i = 0; i < s.GetCount(); i++) {
        ComponentId renderId = { i };
        // TODO: Sort by render order.

        Entity entity = s.GetEntity(renderId);

        ComponentId positionId = p.GetId(entity);

        if (positionId.Id > -1) {
            TextureId textureId = s.GetTextureId(renderId);

            SDL_Texture* texture = textureManager->GetTexture(textureId);

            // TODO: Clean this up.
            Vector tp = s.GetPosition(renderId);
            float a = p.GetAngle(positionId);
            SDL_Rect c = s.GetClip(renderId);

            if (a != 0.0) {
                Vector offset = {(float)c.w / 2, (float)c.h / 2};

                tp += offset;
                tp.Rotate(a * Globals::PI / 180);
                tp -= offset;
            }

            renderer->Render(texture, p.GetPosition(positionId) + tp, c, a);
        }
    }
}
