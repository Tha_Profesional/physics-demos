/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "physicssystem.h"

#include <algorithm>
#include <cmath>

#include "collisiondetection.h"
#include "shapetype.h"

using namespace std;

void PhysicsSystem::seperate(PhysicsComponentManager ph, PositionComponentManager po, CollisionResult cr) {
    ComponentId physA = ph.GetId(cr.EntityA);
    ComponentId physB = ph.GetId(cr.EntityB);

    float massA = ph.GetInverseMass(physA);
    float massB = ph.GetInverseMass(physB);

    if (massA > 0) {
        if (massB > 0) {
            float totalMass = massA + massB;

            ComponentId posA = po.GetId(cr.EntityA);
            ComponentId posB = po.GetId(cr.EntityB);

            po.SetPosition(posA, po.GetPosition(posA) + (cr.Translation * (massA / totalMass)));
            po.SetPosition(posB, po.GetPosition(posB) + (cr.Translation * (massB / totalMass)));
        } else {
            ComponentId posA = po.GetId(cr.EntityA);

            po.SetPosition(posA, po.GetPosition(posA) + cr.Translation);
        }
    } else if (massB > 0) {
        ComponentId posB = po.GetId(cr.EntityB);

        po.SetPosition(posB, po.GetPosition(posB) + cr.Translation);
    }
}

vector<CollisionResult> PhysicsSystem::Check(PhysicsComponentManager ph, PositionComponentManager po, MovementComponentManager m) {
    int count = ph.GetCount();

    vector<CollisionResult> crs;

    for (int i = 0; i < (count - 2); i++) {
        for (int j = i + 1; j < (count - 1); j++) {
            ComponentId a = { i };
            ComponentId b = { j };

            // TODO: did either entity move?

            ShapeType tA = ph.GetShape<Shape>(a).GetType();
            ShapeType tB = ph.GetShape<Shape>(b).GetType();

            CollisionResult cr;

            if (cr.IsCollision)
                crs.push_back(cr);
        }
    }

    return crs;
}

void PhysicsSystem::Separate(PhysicsComponentManager ph, PositionComponentManager po, MovementComponentManager m, vector<CollisionResult> crs) {
    for (auto& i : crs) {
        seperate(ph, po, i);
    }
}
