/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef MOVEMENTSYSTEM_H_INCLUDED
#define MOVEMENTSYSTEM_H_INCLUDED

#include "movementcomponentmanager.h"
#include "positioncomponentmanager.h"
#include "physicscomponentmanager.h"
#include "collisionresult.h"

class MovementSystem {
private:
public:
    void Move(MovementComponentManager& m, PositionComponentManager& p, unsigned ticks);
};

#endif // MOVEMENTSYSTEM_H_INCLUDED


