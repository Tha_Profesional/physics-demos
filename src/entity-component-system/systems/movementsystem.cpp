/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "movementsystem.h"

#include "entity.h"

using namespace std;

void MovementSystem::Move(MovementComponentManager& m, PositionComponentManager& p, unsigned ticks) {
    for (int i = 0; i < m.GetCount(); i++) {
        ComponentId movementId = { i };
        Entity entity = m.GetEntity(movementId);

        ComponentId positionId = p.GetId(entity);

        if (positionId.Id > -1) {
            Vector movement = m.GetVelocity(movementId) * ((float)ticks / 1000);

            p.SetPosition(positionId, p.GetPosition(positionId) + movement);
        }
    }
}
