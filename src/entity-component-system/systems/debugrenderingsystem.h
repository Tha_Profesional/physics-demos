/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef DEBUGRENDERERINGSYSTEM_H_INCLUDED
#define DEBUGRENDERERINGSYSTEM_H_INCLUDED

#include "sdlrenderer.h"
#include "positioncomponentmanager.h"
#include "physicscomponentmanager.h"
#include "vector.h"

class DebugRenderingSystem {
private:
    Window* window;

    void renderCentres(PositionComponentManager& po);
public:
    DebugRenderingSystem(Window* w);

    void Render(PhysicsComponentManager& ph, PositionComponentManager& po);
};

#endif // DEBUGRENDERERINGSYSTEM_H_INCLUDED

