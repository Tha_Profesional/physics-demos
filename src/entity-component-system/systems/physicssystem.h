/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef PHYSICSSYSTEM_H_INCLUDED
#define PHYSICSSYSTEM_H_INCLUDED

#include <vector>

#include "physicscomponentmanager.h"
#include "positioncomponentmanager.h"
#include "movementcomponentmanager.h"
#include "componentid.h"
#include "collisionresult.h"

class PhysicsSystem
{
private:
    void seperate(PhysicsComponentManager ph, PositionComponentManager po, CollisionResult cr);
public:
    std::vector<CollisionResult> Check(PhysicsComponentManager ph, PositionComponentManager po, MovementComponentManager m);

    void Separate(PhysicsComponentManager ph, PositionComponentManager po, MovementComponentManager m, std::vector<CollisionResult> crs);
};

#endif // PHYSICSSYSTEM_H_INCLUDED

