/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "debugrenderingsystem.h"

#include "SDL2/SDL.h"

using namespace std;

DebugRenderingSystem::DebugRenderingSystem(Window* w) : window(w) {}

void DebugRenderingSystem::renderCentres(PositionComponentManager& po) {
    for (int i = 0; i < po.GetCount(); i++) {
        ComponentId positionId = { i };

        Vector p = po.GetPosition(positionId);

        Vector c1 = p - 2;
        Vector c2 = p + 2;
        Vector c3 = p - Vector(2, -2);
        Vector c4 = p - Vector(-2, 2);

        SDL_SetRenderDrawColor(window->GetRenderer(), 0, 255, 0, 1);

        SDL_RenderDrawLine(window->GetRenderer(), c1.X, c1.Y, c2.X, c2.Y);
        SDL_RenderDrawLine(window->GetRenderer(), c3.X, c3.Y, c4.X, c4.Y);
    }
}

void DebugRenderingSystem::Render(PhysicsComponentManager& ph, PositionComponentManager& po) {
    renderCentres(po);
}
