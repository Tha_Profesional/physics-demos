# ---------------- #
# add source files #
# ---------------- #
set(entity-component-system_source_files
    entity.h
)

# ----------------------- #
# add include directories #
# ----------------------- #
set(entity-component-system_include_directories
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

# ------------------- #
# add sub-directories #
# ------------------- #
add_subdirectory(components)
add_subdirectory(managers)
add_subdirectory(systems)

set(entity-component-system_source_files
    ${entity-component-system_source_files}
    ${components_source_files}
    ${managers_source_files}
    ${systems_source_files}
)

set(entity-component-system_include_directories
    ${entity-component-system_include_directories}
    ${components_include_directories}
    ${managers_include_directories}
    ${systems_include_directories}
)

# -------------- #
# make a library #
# -------------- #
add_library(entity-component-system
    ${entity-component-system_source_files}
)

target_include_directories(entity-component-system PUBLIC "${entity-component-system_include_directories}")

target_link_libraries(entity-component-system
    base
    physics
    rendering
    utilities
)
