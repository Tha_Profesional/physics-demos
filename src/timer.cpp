/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "timer.h"

#include "SDL2/SDL.h"

Timer::Timer() {
    started = false;
    paused = false;

    startTicks = 0;
    pausedTicks = 0;
}

void Timer::Start() {
    started = true;
    paused  = false;

    startTicks = SDL_GetTicks();
}

void Timer::Stop() {
    started = false;
    paused  = false;
}

void Timer::Pause() {
    if (started && !paused){
        paused = true;
        pausedTicks = SDL_GetTicks() - startTicks;
    }
}

void Timer::Unpause() {
    if (paused){
        paused = false;
        startTicks = SDL_GetTicks() - pausedTicks;
        pausedTicks = 0;
    }
}

int Timer::Restart() {
    int elapsedTicks = Ticks();

    Start();

    return elapsedTicks;
}

int Timer::Ticks() const {
    if (started) {
        if (paused)
            return pausedTicks;
        else
            return SDL_GetTicks() - startTicks;
    }

    return 0;
}

bool Timer::Started() const {
    return started;
}

bool Timer::Paused() const {
    return paused;
}
