/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "satdetection.h"

#include "satdetectionutilities.h"
#include "vectorutilities.h"

using namespace std;

SATResult RectangleVsCircle(const Rectangle& rectangle, const Circle& circle) {
    SATResult satResult;

    Vector rectangleWidth = rectangle.GetTopVector();
    Vector rectangleHeight = rectangle.GetLeftVector();

    Vector centreDistance = circle.Centre - rectangle.GetCentre();

    float rectangleWidthMagnitude = rectangleWidth.Magnitude();
    float rectangleHeightMagnitude = rectangleHeight.Magnitude();

    Vector rectangleWidthUnit = VectorUtilities::Unit(rectangleWidth, rectangleWidthMagnitude);
    Vector rectangleHeightUnit = VectorUtilities::Unit(rectangleHeight, rectangleHeightMagnitude);

    bool verticalTest = SATDetectionUtilities::CircleVsRectangleEdgeCheck(
        rectangleHeightMagnitude,
        rectangleHeightUnit,
        centreDistance);
    bool horizontalTest = SATDetectionUtilities::CircleVsRectangleEdgeCheck(
        rectangleWidthMagnitude,
        rectangleWidthUnit,
        centreDistance);

    if (verticalTest || horizontalTest) {
        float circleRadius = circle.Radius;
        Vector projectedRadius = centreDistance.Unit() * circleRadius;

        if (verticalTest) {
            SATDetectionUtilities::CircleVsRectangleCheckSides(
                centreDistance,
                circleRadius,
                rectangleWidthUnit,
                rectangleWidthMagnitude,
                &satResult);
        }

        if (horizontalTest) {
            SATDetectionUtilities::CircleVsRectangleCheckSides(
                centreDistance,
                circleRadius,
                rectangleHeightUnit,
                rectangleHeightMagnitude,
                &satResult);
        }
    } else {
        SATDetectionUtilities::CircleVsRectangleCheckDiagonals(
            circle,
            rectangle,
            centreDistance,
            &satResult);
    }

    return satResult;
}
