/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "satdetectionutilities.h"

#include <cmath>

#include "line.h"
#include "quickdetection.h"

using namespace std;

bool SATDetectionUtilities::CircleVsRectangleEdgeCheck(float magnitude, Vector unit, Vector centreDistance) {
    float p = unit.DotProduct(centreDistance);
    return p > 0 && p < magnitude;
}

void SATDetectionUtilities::CircleVsRectangleCheckSides(Vector cD, float cR, Vector sU, float sM, SATResult* sr) {
    float m = sU.DotProduct(cD);

    float offset = abs(m) - cR - (sM * 0.5);

    if (offset < 0) {
        float aOffset = abs(offset);

        if (sr->IsCollision) {
            if (sr->Translation.Magnitude() > aOffset) {
                sr->Axis = m < 0 ? sU * -1 : sU;
                sr->Translation = sr->Axis * aOffset;
            }
        } else {
            sr->IsCollision = true;
            sr->Axis = m < 0 ? sU * -1 : sU;
            sr->Translation = sr->Axis * aOffset;
        }
    }
}

void SATDetectionUtilities::CircleVsRectangleCheckDiagonals(Circle c, Rectangle r, Vector cD, SATResult* sr) {
    // figure out which corner is closest to the circle
    Vector uOffset = cD.Unit();

    float max = uOffset.DotProduct(r.GetCorner(0) - r.GetCentre());
    int corner = 0;

    for (int i = 1; i < 4; i++) {
        float p = uOffset.DotProduct(r.GetCorner(i) - r.GetCentre());

        if (max < p) {
            max = p;
            corner = i;
        }
    }

    // check collision vs the correct corner
    Vector cC = c.Centre - r.GetCorner(corner);
    Vector cCU = cC.Unit();
    float cCM = cC.Magnitude();

    float offset = abs(cCM) - c.Radius;

    if (offset < 0) {
        float aOffset = abs(offset);

        sr->IsCollision = true;

        sr->Axis = cCM < 0 ? cCU * -1 : cCU;
        sr->Translation = sr->Axis * aOffset;
    }
}
