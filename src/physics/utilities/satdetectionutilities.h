/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef SATDETECTIONUTILS_H_INCLUDED
#define SATDETECTIONUTILS_H_INCLUDED

#include "circle.h"
#include "rectangle.h"
#include "satresult.h"
#include "vector.h"

namespace SATDetectionUtilities {
    bool CircleVsRectangleEdgeCheck(float magnitude, Vector unit, Vector centreDistance);

    void CircleVsRectangleCheckSides(Vector centreDistance, float circleRadius, Vector sideUnit, float sideMagnitude, SATResult* satResult);
    void CircleVsRectangleCheckDiagonals(Circle circle, Rectangle rectangle, Vector centreDistance, SATResult* satResult);
};

#endif // SATDETECTIONUTILS_H_INCLUDED

