/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef VECTORUTILITIES_H_INCLUDED
#define VECTORUTILITIES_H_INCLUDED

#include "vector.h"

namespace VectorUtilities {
    bool Collinear(Vector vt, Vector v1, Vector v2);
    Vector Unit(Vector v, float m);
}

#endif // VECTORUTILITIES_H_INCLUDED

