/*
 * Special thanks to:
 *   - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "vectorutils.h"

#include <cmath>

#include "mathutilities.h"

using namespace std;

// TODO: Replace with original implementation.
bool VectorUtilities::Collinear(Vector vt, Vector v1, Vector v2) {
    float t1 = (v1.Y - vt.Y) * (v2.X - v1.X);
    float t2 = (v2.Y - v1.Y) * (v1.X - vt.X);

    return MathUtils::Equal(t1, t2);
}

Vector VectorUtilities::Unit(Vector v, float m) {
    return {(v.X / m), (v.Y / m)};
}
