/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "collisiondetection.h"

#include "mathutilities.h"
#include "satdetection.h"
#include "satresult.h"

using namespace std;

CollisionResult CollisionDetection::CircleVsRectangle(const Circle& circle, const Rectangle& rectangle) {
    CollisionResult collisionResult;

    SATResult satResult;

    satResult = SATDetection::RectangleVsCircle(rectangle, circle);

    if (satResult.IsCollision) {
        collisionResult.IsCollision = true;
        collisionResult.Translation = satResult.Translation;
        collisionResult.Axis = satResult.Axis;
    }

    return collisionResult;
}
