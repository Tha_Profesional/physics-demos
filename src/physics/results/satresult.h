/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef SATRESULT_H_INCLUDED
#define SATRESULT_H_INCLUDED

#include "vector.h"

struct SATResult {
    bool IsCollision;

    Vector Translation;
    Vector Axis;

    SATResult(bool ic = false) : IsCollision(ic) {};
};

#endif // SATRESULT_H_INCLUDED
