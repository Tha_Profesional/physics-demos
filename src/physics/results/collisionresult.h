/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef COLLISIONRESULT_H_INCLUDED
#define COLLISIONRESULT_H_INCLUDED

#include "entity.h"
#include "vector.h"

struct CollisionResult {
    bool IsCollision;

    Entity EntityA;
    Entity EntityB;

    Vector Translation;
    Vector Axis;

    float Percent; // TODO: Is this needed / used?

    CollisionResult(bool isCollision = false);
};

#endif // COLLISIONRESULT_H_INCLUDED
