/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef QUICKDETECTION_H_INCLUDED
#define QUICKDETECTION_H_INCLUDED

#include "circle.h"
#include "line.h"

namespace QuickDetection {
    bool LineVsLine(Line lA, Line lB);
    bool CircleVsLine(Circle c, Line l);

    bool PointWithinLine(Line l, Vector v);
};

#endif // QUICKDETECTION_H_INCLUDED

