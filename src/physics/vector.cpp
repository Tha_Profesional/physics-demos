/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 		- None
 */

#include "vector.h"

#include <cmath>

#include "mathutilities.h"

using namespace std;

Vector::Vector() : X(0), Y(0) {}

Vector::Vector(float aX, float aY) : X(aX), Y(aY) {}

float Vector::Magnitude() {
    return sqrt((X * X) + (Y * Y));
}

Vector Vector::Unit() {
    Vector aVector = *this;

    if (aVector != Vector()) {
        float m = Magnitude();

        aVector.X = aVector.X / m;
        aVector.Y = aVector.Y / m;
    }

    return aVector;
}

Vector Vector::LeftNormal() {
    Vector aVector = *this;

    aVector.X = Y * -1;
    aVector.Y = X;

    return aVector;
}

Vector Vector::RightNormal() {
    Vector aVector = *this;

    aVector.X = Y;
    aVector.Y = X * -1;

    return aVector;
}

Vector Vector::Absolute() {
    return {abs(X), abs(Y)};
}

Vector Vector::Project(Vector& aVector) {
    Vector bVector = *this;

    float dab = aVector.DotProduct(bVector);
    float daa = aVector.DotProduct(aVector);

    return aVector * (dab / daa);
}

void Vector::Rotate(float degreeRad) {
    float pX = (cos(degreeRad) * X) - (sin(degreeRad) * Y);
    float pY = (sin(degreeRad) * X) + (cos(degreeRad) * Y);

    X = pX;
    Y = pY;
}

float Vector::DotProduct(const Vector& aVector) {
    return ((X * aVector.X) + (Y * aVector.Y));
}

float Vector::CrossProduct(const Vector& aVector) {
    return ((X * aVector.Y) - (Y * aVector.X));
}

Vector& Vector::operator=(const Vector& aVector) {
    X = aVector.X;
    Y = aVector.Y;

    return *this;
}

Vector Vector::operator*(const float scalarValue) const {
    Vector bVector = *this;

    bVector.X *= scalarValue;
    bVector.Y *= scalarValue;

    return bVector;
}

Vector Vector::operator+(const float scalarValue) const {
    Vector bVector = *this;

    bVector.X += scalarValue;
    bVector.Y += scalarValue;

    return bVector;
}

Vector Vector::operator-(const float scalarValue) const {
    Vector bVector = *this;

    bVector.X -= scalarValue;
    bVector.Y -= scalarValue;

    return bVector;
}

Vector Vector::operator*=(const float scalarValue) {
    *this = *this * scalarValue;
}

Vector Vector::operator*(const Vector& aVector) const {
    Vector bVector = *this;

    bVector.X *= aVector.X;
    bVector.Y *= aVector.Y;

    return bVector;
}

Vector Vector::operator+(const Vector& aVector) const {
    Vector bVector = *this;

    bVector.X += aVector.X;
    bVector.Y += aVector.Y;

    return bVector;
}

Vector Vector::operator-(const Vector& aVector) const {
    Vector bVector = *this;

    bVector.X -= aVector.X;
    bVector.Y -= aVector.Y;

    return bVector;
}

Vector Vector::operator+=(const Vector& aVector) {
    *this = *this + aVector;
}


Vector Vector::operator-=(const Vector& aVector) {
    *this = *this - aVector;
}

bool Vector::operator==(const Vector& aVector) const {
    Vector bVector = *this;

    return MathUtilities::Equal(aVector.X, bVector.X) && MathUtilities::Equal(aVector.Y, bVector.Y);
}

bool Vector::operator!=(const Vector& aVector) const {
    Vector bVector = *this;

    return !MathUtilities::Equal(aVector.X, bVector.X) || !MathUtilities::Equal(aVector.Y, bVector.Y);
}
