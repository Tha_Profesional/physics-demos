/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <string>

struct Vector {
    float X;
    float Y;

    Vector();
    Vector(float aX, float aY);

    float Magnitude();

    Vector Unit();
    Vector LeftNormal();
    Vector RightNormal();

    Vector Absolute();

    Vector Project(Vector& aVector);

    void Rotate(float degreeRad);
    float DotProduct(const Vector& aVector);
    float CrossProduct(const Vector& aVector);

    Vector& operator=(const Vector& aVector);

    Vector operator*(const float scalarValue) const;
    Vector operator+(const float scalarValue) const;
    Vector operator-(const float scalarValue) const;

    Vector operator*=(const float scalarValue);

    Vector operator*(const Vector& aVector) const;
    Vector operator+(const Vector& aVector) const;
    Vector operator-(const Vector& aVector) const;

    Vector operator+=(const Vector& aVector);
    Vector operator-=(const Vector& aVector);

    bool operator==(const Vector& aVector) const;
    bool operator!=(const Vector& aVector) const;
};

#endif // VECTOR_H_INCLUDED
