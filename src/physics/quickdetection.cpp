/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "quickdetection.h"

#include "mathutilities.h"
#include "vectorutilities.h"

using namespace std;

bool QuickDetection::LineVsLine(Line lineA, Line lineB) {
    // Code from: http://gamedev.stackexchange.com/questions/26004/how-to-detect-2d-line-on-line-collision
    Vector a = lineA.A;
    Vector b = lineA.B;
    Vector c = lineB.A;
    Vector d = lineB.B;

    float denominator = ((b.X - a.X) * (d.Y - c.Y)) - ((b.Y - a.Y) * (d.X - c.X));
    float numerator1 = ((a.Y - c.Y) * (d.X - c.X)) - ((a.X - c.X) * (d.Y - c.Y));
    float numerator2 = ((a.Y - c.Y) * (b.X - a.X)) - ((a.X - c.X) * (b.Y - a.Y));

    if (denominator == 0) {
        return (numerator1 == 0 && numerator2 == 0);
    } else {
        float r = numerator1 / denominator;
        float s = numerator2 / denominator;

        return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
    }
}

bool QuickDetection::CircleVsLine(Circle circle, Line line) {
    // http://www.gamedev.net/topic/433765-simple-2d-ball-line-collision/
    Vector circleCentre = circle.Centre;
    float circleRadius = circle.Radius;

    Vector linePointA = l.A;
    Vector linePointB = l.B;

    Vector aC = cC - linePointA;
    Vector aB = linePointB - linePointA;

    float aB2 = aB.DotProduct(aB);
    float aCAB = aC.DotProduct(aB);

    float t = aCAB / aB2;

    if (t < 0.0f)
        t = 0.0f;
    else if (t > 1.0f)
        t = 1.0f;

    Vector p = linePointA + aB * t;
    Vector h = p - cC;

    float h2 = h.DotProduct(h);
    float cR2 = cR * cR;

    return h2 <= cR2;
}

bool QuickDetection::PointWithinLine(Line l, Vector v) {
    // http://stackoverflow.com/questions/328107/how-can-you-determine-a-point-is-between-two-other-points-on-a-line-segment
    Vector p1 = l.A;
    Vector p2 = l.B;

    if (!MathUtilities::Within(v.X, p1.X, p2.X) || !MathUtilities::Within(v.Y, p1.Y, p2.Y))
        return false;

    return VectorUtilities::Collinear(v, p1, p2);
}
