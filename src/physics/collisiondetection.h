/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef COLLISIONDETECTION_H_INCLUDED
#define COLLISIONDETECTION_H_INCLUDED

#include "circle.h"
#include "collisionresult.h" 
#include "rectangle.h"

namespace CollisionDetection {
    CollisionResult CircleVsRectangle(const Circle& c, const Rectangle& r);
};

#endif // COLLISIONDETECTION_H_INCLUDED

