/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef SHAPE_H_INCLUDED
#define SHAPE_H_INCLUDED

#include "shapetype.h"

class Shape {
protected:
    ShapeType type;
public:
    Shape();

    Shape(ShapeType shapeType);

    ShapeType GetType();
};

#endif // SHAPE_H_INCLUDED

