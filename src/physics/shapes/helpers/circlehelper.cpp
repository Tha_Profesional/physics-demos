/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "circlehelper.h"

using namespace std;

Circle CircleHelper::GetAdjusted(PhysicsComponentManager& ph, PositionComponentManager& po, ComponentId cId) {
    Circle c = ph.GetShape<Circle>(cId);

    Entity e = ph.GetEntity(cId);

    ComponentId pId = po.GetId(e);

    c.Move(po.GetPosition(pId));

    return c;
}

Vector CircleHelper::GetTopDistance(Circle c, Line v) {
    return v.GetVector().LeftNormal().Unit() * c.Radius;
}

Vector CircleHelper::GetBottomDistance(Circle c, Line v) {
    return v.GetVector().RightNormal().Unit() * c.Radius;
}

Line CircleHelper::GetRay(Circle c, Vector v) {
    // http://stackoverflow.com/questions/7740507/extend-a-line-segment-a-specific-distance
    return Line(c.Centre, c.Centre + v);
}

Line CircleHelper::GetRayCentre(Circle c, Vector v) {
    Line l = GetRay(c, v);

    float r = c.Radius;

    l.ExtendA(r);
    l.ExtendB(r);

    return l;
}

Line CircleHelper::GetTopRay(Circle c, Line cr) {
    Vector ctd = GetTopDistance(c, cr);

    cr.Move(ctd);

    return cr;
}

Line CircleHelper::GetBottomRay(Circle c, Line cr) {
    Vector ctd = GetBottomDistance(c, cr);

    cr.Move(ctd);

    return cr;
}

