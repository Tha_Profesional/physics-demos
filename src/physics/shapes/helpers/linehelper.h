/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef LINEHELPER_H_INCLUDED
#define LINEHELPER_H_INCLUDED

#include "componentid.h"
#include "line.h"
#include "physicscomponentmanager.h"
#include "positioncomponentmanager.h"

namespace LineHelper {
    Line GetAdjusted(PhysicsComponentManager& ph, PositionComponentManager& po, ComponentId cId);
}

#endif // LINEHELPER_H_INCLUDED
