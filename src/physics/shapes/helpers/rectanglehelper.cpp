/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "rectanglehelper.h"

using namespace std;

Rectangle RectangleHelper::GetAdjusted(PhysicsComponentManager& ph, PositionComponentManager& po, ComponentId cId) {
    Rectangle r = ph.GetShape<Rectangle>(cId);

    Entity e = ph.GetEntity(cId);

    ComponentId pId = po.GetId(e);

    float a = po.GetAngle(pId) * Globals::PI / 180;

    r.Rotate(a);

    r.Move(po.GetPosition(pId));

    return r;
}
