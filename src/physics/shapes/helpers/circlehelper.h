/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef CIRCLEHELPER_H_INCLUDED
#define CIRCLEHELPER_H_INCLUDED

#include "circle.h"
#include "line.h"
#include "componentid.h"
#include "positioncomponentmanager.h"
#include "physicscomponentmanager.h"

namespace CircleHelper {
    Circle GetAdjusted(PhysicsComponentManager& ph, PositionComponentManager& po, ComponentId cId);

    Vector GetTopDistance(Circle c, Line l);
    Vector GetBottomDistance(Circle c, Line l);

    Line GetRay(Circle c, Vector v);
    Line GetRayCentre(Circle c, Vector v);

    Line GetTopRay(Circle c, Line cr);
    Line GetBottomRay(Circle c, Line cr);
};

#endif // CIRCLEHELPER_H_INCLUDED
