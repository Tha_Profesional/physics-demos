/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "linehelper.h"

using namespace std;

Line LineHelper::GetAdjusted(PhysicsComponentManager& ph, PositionComponentManager& po, ComponentId cId) {
    Line l = ph.GetShape<Line>(cId);

    Entity e = ph.GetEntity(cId);

    ComponentId pId = po.GetId(e);

    l.Move(po.GetPosition(pId));

    return l;
}
