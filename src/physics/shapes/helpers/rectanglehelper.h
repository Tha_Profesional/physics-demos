/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef RECTANGLEHELPER_H_INCLUDED
#define RECTANGLEHELPER_H_INCLUDED

#include "componentid.h"
#include "physicscomponentmanager.h"
#include "positioncomponentmanager.h"
#include "rectangle.h"

namespace RectangleHelper {
    Rectangle GetAdjusted(PhysicsComponentManager& ph, PositionComponentManager& po, ComponentId cId);
}

#endif // RECTANGLEHELPER_H_INCLUDED

