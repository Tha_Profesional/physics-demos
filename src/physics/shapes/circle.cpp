/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#include "circle.h"

using namespace std;

Circle::Circle() : Shape(ShapeType::Circle) {}

Circle::Circle(float r, Vector c) : Shape(ShapeType::Circle), Centre(c), Radius(r) {}

void Circle::Move(const Vector offset) {
    Centre += offset;
}

