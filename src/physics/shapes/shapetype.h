/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef SHAPETYPE_H_INCLUDED
#define SHAPETYPE_H_INCLUDED

enum class ShapeType {
    None,
    Line,
    Rectangle,
    Circle
};

#endif // SHAPETYPE_H_INCLUDED
