/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "shape.h"

using namespace std;

Shape::Shape() : type(ShapeType::None) {}

Shape::Shape(ShapeType shapeType) : type(shapeType) {}

ShapeType Shape::GetType() {
    return type;
}
