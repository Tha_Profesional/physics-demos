/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef CIRCLE_H_INCLUDED
#define CIRCLE_H_INCLUDED

#include "shape.h"
#include "shapetype.h"
#include "vector.h"

class Circle : public Shape {
public:
    Vector Centre;
    float Radius;

    Circle();
    Circle(float r, Vector c);

    void Move(const Vector offset);
};

#endif // CIRCLE_H_INCLUDED

