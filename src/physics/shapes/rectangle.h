/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef RECTANGLE_H_INCLUDED
#define RECTANGLE_H_INCLUDED

#include "shape.h"
#include "vector.h"
#include "shapetype.h"

class Rectangle : public Shape {
private:
    float width;
    float height;
    float angle;

    Vector centre;

    Vector corners[4];
public:
    Rectangle();
    Rectangle(float w, float h, Vector c, float a);

    void SetSize(float w, float h);
    void Rotate(float a);
    void Move(Vector offset);

    float GetWidth() const;
    float GetHeight() const;
    float GetAngle() const;
    Vector GetCentre() const;

    Vector GetCorner(int i) const;
    Vector GetTopVector() const;
    Vector GetLeftVector() const;
};

#endif // RECTANGLE_H_INCLUDED
