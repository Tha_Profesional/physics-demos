/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "rectangle.h"

#include <cmath>
#include <stdexcept>

#include "globals.h"

using namespace std;

// TODO: Clean this up once there's better entity loading :)

Rectangle::Rectangle() : Shape(ShapeType::Rectangle),
    width(0), height(0), angle(0), centre({0, 0}) {}

Rectangle::Rectangle(float w, float h, Vector c, float a) : Shape(ShapeType::Rectangle) {
    centre = c;

    SetSize(w, h);

    Rotate(a);
}

void Rectangle::SetSize(float w, float h) {
    width = w;
    height = h;

    float a = angle;

    float halfWidth = width / 2;
    float halfHeight = height / 2;

    corners[0] = centre + Vector(halfWidth * -1 ,halfHeight * -1);
    corners[1] = centre + Vector(halfWidth ,halfHeight * -1);
    corners[2] = centre + Vector(halfWidth ,halfHeight);
    corners[3] = centre + Vector(halfWidth * -1 ,halfHeight);

    if (a != 0) {
        angle = 0;
        Rotate(a);
    }
}

void Rectangle::Rotate(float a) {
    Vector c = centre;

    for (int i = 0; i < 4; i++) {
        corners[i] -= centre;
        corners[i].Rotate(a);
        corners[i] += centre;
    }

    angle += a;

    int rem = angle / Globals::TWO_PI_RAD;

    if (rem != 0)
        angle -= rem * Globals::TWO_PI_RAD;
}

void Rectangle::Move(const Vector offset) {
    for(int i = 0; i < 4; i++)
        corners[i] += offset;

    centre += offset;
}

float Rectangle::GetWidth() const {
    return width;
}

float Rectangle::GetHeight() const {
    return height;
}

float Rectangle::GetAngle() const {
    return angle;
}

Vector Rectangle::GetCentre() const {
    return centre;
}

Vector Rectangle::GetCorner(int i) const {
    return corners[i];
}

Vector Rectangle::GetTopVector() const {
    return corners[1] - corners[0];
}

Vector Rectangle::GetLeftVector() const {
    return corners[3] - corners[0];
}
