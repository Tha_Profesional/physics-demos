/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#include "line.h"

using namespace std;

Line::Line() : Shape(ShapeType::Line) {}

Line::Line(Vector a, Vector b) : Shape(ShapeType::Line), A(a), B(b) {}

void Line::ExtendA(float l) {
    float lenAB = GetLength();

    Vector v;

    v.X = A.X + (A.X - B.X) / (lenAB * l);
    v.Y = A.Y + (A.Y - B.Y) / (lenAB * l);

    A = v;
}

void Line::ExtendB(float l) {
    float lenAB = GetLength();

    Vector v;

    v.X = B.X + (B.X - A.X) / lenAB * l;
    v.Y = B.Y + (B.Y - A.Y) / lenAB * l;

    B = v;
}

float Line::GetLength() {
    Vector v = B - A;

    return v.Magnitude();
}

Vector Line::GetCentre() {
    float x = (A.X + B.X) / 2;
    float y = (A.Y + B.Y) / 2;

    return Vector(x, y);
}

Vector Line::GetVector() {
    return B - A;
}

void Line::GetABC(float* a, float* b, float* c) {
    *a = B.Y - A.Y;
    *b = A.X - B.X;
    *c = (*a * A.X) + (*b * A.Y);
}

