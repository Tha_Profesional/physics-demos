/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef LINE_H_INCLUDED
#define LINE_H_INCLUDED

#include "shape.h"
#include "vector.h"
#include "shapetype.h"

class Line : public Shape {
public:
    Vector A;
    Vector B;

    Line();
    Line(Vector a, Vector b);

    void Move(const Vector offset);

    void ExtendA(float l);
    void ExtendB(float l);

    float GetLength();
    Vector GetCentre();
    Vector GetVector();
    void GetABC(float* a, float* b, float* c);
};

#endif // LINE_H_INCLUDED


