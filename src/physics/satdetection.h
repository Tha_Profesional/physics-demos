/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef SATDETECTION_H_INCLUDED
#define SATDETECTION_H_INCLUDED

#include "satresult.h"
#include "rectangle.h"
#include "circle.h"

namespace SATDetection {
    SATResult RectangleVsCircle(const Rectangle& rectangle, const Circle& circle);
};

#endif // SATDETECTION_H_INCLUDED


