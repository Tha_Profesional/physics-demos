/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef STATEMANAGER_H_INCLUDED
#define STATEMANAGER_H_INCLUDED

#include <deque>

#include "SDL2/SDL.h"

class State;

class StateManager
{
private:
    std::deque<State*> stateStack;
public:
    StateManager();
    ~StateManager();

    void Input(SDL_Event& event);
    void Input();
    void Update(int ticks);
    void Render();

    void AddState(State* newState);
    void RemoveState();

    int GetStateCount();
    void RemoveAllStates();
};

#endif // STATEMANAGER_H_INCLUDED
