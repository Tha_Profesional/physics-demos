/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef STATE_H_INCLUDED
#define STATE_H_INCLUDED

#include "SDL2/SDL.h"

#include "statemanager.h"
#include "window.h"
#include "timer.h"

class State
{
protected:
    StateManager* stateManager;
    Window* window;
    Timer* timer;
public:
    State(StateManager* manager, Window* win, Timer* t) : stateManager(manager), window(win), timer(t) {};
    virtual ~State() {};

    virtual void Input(SDL_Event& event) = 0;
    virtual void Input() = 0;
    virtual void Update(unsigned ticks) = 0;
    virtual void Render() = 0;
};

#endif // STATE_H_INCLUDED
