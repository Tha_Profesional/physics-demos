/*
 * special thanks to:
 *  - no-one yet
 *
 * notes:
 * 	- none
 */

#ifndef STATE_H_INCLUDED
#define STATE_H_INCLUDED

#include "SDL2/SDL.h"

#include "statemanager.h"
#include "window.h"
#include "timer.h"

class Spike : public State
{
protected:
public:
    Spike(StateManager* sm, Window* w, Timer* t);
    ~Spike();

    void Input(SDL_Event& event);
    void Input();

    void Update(unsigned ticks);

    void Render();
};

#endif // STATE_H_INCLUDED
