/* 
 * Special thanks to:
 *  - No-one yet.
 *
 * Notes:
 *  - None yet.
 */

#ifndef DEMO_H_INCLUDED
#define DEMO_H_INCLUDED

#include "debugrenderingsystem.h"
#include "entitymanager.h"
#include "movementcomponentmanager.h"
#include "movementsystem.h"
#include "physicscomponentmanager.h"
#include "physicssystem.h"
#include "positioncomponentmanager.h"
#include "renderingsystem.h"
#include "sdlrenderer.h"
#include "state.h"
#include "statemanager.h"
#include "staticrendercomponentmanager.h"
#include "texturemanager.h"
#include "timer.h"
#include "window.h"

class CircleVs : public State {
private:
    SDLRenderer renderer;

    EntityManager entityManager;

    MovementComponentManager movementComponentManager;
    PhysicsComponentManager physicsComponentManager;
    PositionComponentManager positionComponentManager;
    StaticRenderComponentManager staticRenderComponentManager;

    TextureManager textureManager;

    DebugRenderingSystem debugRenderingSystem;
    MovementSystem movementSystem;
    PhysicsSystem physicsSystem;
    RenderingSystem renderingSystem;

    void createLevel();
    void createWall(TextureId textureId, int width, int height, Vector position);
    void destroyCircle();
    void spawnCircle();
public:
    CircleVs(StateManager* stateManager, Window* window, Timer* timer);
    ~CircleVs();

    void Input(SDL_Event& event);
    void Input();
    void Update(unsigned ticks);
    void Render();
};

#endif // DEMO_H_INCLUDED
