/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "circlevs.h"

#include "circle.h"
#include "rectangle.h"

using namespace std;

CircleVs::CircleVs(StateManager* stateManager, Window* window, Timer* timer) :
    State(stateManager, window, timer),
    debugRenderingSystem(window),
    renderer(window),
    textureManager(window), 
    renderingSystem(&renderer, &textureManager)
{
    createLevel();

    spawnCircle();
}

CircleVs::~CircleVs() {}

void CircleVs::createLevel() {
    TextureId horizontalWallTextureId = textureManager.Create("assets/terrain/horizontal-wall.png");

    createWall(horizontalWallTextureId, 1174, 80, {587, 0}); // top
    createWall(horizontalWallTextureId, 1174, 80, {587, 660}); // bottom

    TextureId verticalWallTextureId = textureManager.Create("assets/terrain/vertical-wall.png");

    createWall(verticalWallTextureId, 80, 660, {0, 330}); // left
    createWall(verticalWallTextureId, 80, 660, {1174, 330}); // right
}

void CircleVs::createWall(TextureId textureId, int width, int height, Vector position) {
    Vector centreOffset = Vector(width, height) * -0.5;

    Entity wall = entityManager.Create();

    ComponentId positionComponentId = positionComponentManager.Create(wall);
    positionComponentManager.SetPosition(positionComponentId, position);
    
    ComponentId staticRenderComponentId = staticRenderComponentManager.Create(wall);
    staticRenderComponentManager.SetTextureId(staticRenderComponentId, textureId);
    staticRenderComponentManager.SetPosition(staticRenderComponentId, centreOffset);
    staticRenderComponentManager.SetClip(staticRenderComponentId, {0, 0, width, height});
    
    ComponentId physicsComponentId = physicsComponentManager.Create(wall);

    Rectangle collisionRectangle(width, height, centreOffset, 0);

    physicsComponentManager.SetShape(physicsComponentId, collisionRectangle);
    physicsComponentManager.SetInverseMass(physicsComponentId, 0);
    physicsComponentManager.SetElasticity(physicsComponentId, 0.5);
}

void CircleVs::destroyCircle() {
    // get random entity
    
    // remove everything
}

void CircleVs::spawnCircle() {
    Entity circle = entityManager.Create();

    TextureId circleTextureId = textureManager.Create("assets/ball.png");
    
    ComponentId staticRenderComponentId = staticRenderComponentManager.Create(circle);
    staticRenderComponentManager.SetTextureId(staticRenderComponentId, circleTextureId);
    staticRenderComponentManager.SetPosition(staticRenderComponentId, {-20, -20});
    staticRenderComponentManager.SetClip(staticRenderComponentId, {0, 0, 40, 40});
    
    ComponentId physicsComponentId = physicsComponentManager.Create(circle);

    Circle collisionCircle(40, {0, 0});

    physicsComponentManager.SetShape(physicsComponentId, collisionCircle);
    physicsComponentManager.SetInverseMass(physicsComponentId, 0.5);
    physicsComponentManager.SetElasticity(physicsComponentId, 0.5);

    ComponentId positionComponentId = positionComponentManager.Create(circle);
    positionComponentManager.SetPosition(positionComponentId, {100, 100});
    
    ComponentId movementComponentId = movementComponentManager.Create(circle);
    movementComponentManager.SetVelocity(movementComponentId, {100, 100});
}

void CircleVs::Input(SDL_Event& event) {

}

void CircleVs::Input() {

}

void CircleVs::Update(unsigned ticks) {
    movementSystem.Move(movementComponentManager, positionComponentManager, ticks);

    vector<CollisionResult> collisionResults = physicsSystem.Check(physicsComponentManager, positionComponentManager, movementComponentManager);
}

void CircleVs::Render() {
    window->ClearRenderer();

    renderingSystem.Render(staticRenderComponentManager, positionComponentManager);

    window->Render();
}

