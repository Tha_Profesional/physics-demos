/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "statemanager.h"

#include "state.h"

using namespace std;

StateManager::StateManager() {}

StateManager::~StateManager() {}

void StateManager::Input(SDL_Event& event) {
    if (!stateStack.empty()) {
        stateStack.back()->Input(event);
    }
}

void StateManager::Input() {
    if (!stateStack.empty()) {
        stateStack.back()->Input();
    }
}

void StateManager::Update(int ticks) {
    if (!stateStack.empty()) {
        stateStack.back()->Update(ticks);
    }
}

void StateManager::Render() {
    if (!stateStack.empty()) {
        stateStack.back()->Render();
    }
}

void StateManager::AddState(State* newState) {
    stateStack.push_back(newState);
}

void StateManager::RemoveState() {
    stateStack.pop_back();
}

int StateManager::GetStateCount() {
    return stateStack.size();
}

void StateManager::RemoveAllStates() {
    stateStack.clear();
}
