/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "mathutilities.h"

#include <cmath>
#include <random>

#include "comparablefloat.h"

using namespace std;

bool MathUtilities::Within(float nt, float n1, float n2) {
    return (n1 <= nt && nt <= n2) || (n2 <= nt && nt <= n1);
}

bool MathUtilities::Equal(float n1, float n2) {
    return (fabs(n1) < 1 && fabs(n2) < 1)
        ? EqualRelative(n1, n2, 0.0009, 0.01)
        : EqualUlps(n1, n2, 0.0009, 2);
}

bool MathUtilities::EqualUlps(float n1, float n2, float maxDiff, int maxUlpsDiff) {
    // Check to see if the numbers are really close
    float diff = fabs(n1 - n2);

    if (diff <= maxDiff)
        return true;

    ComparableFloat a(n1);
    ComparableFloat b(n2);

    // Different signs means they do not match.
    if (a.Negative() != b.Negative())
        return false;

    // Find the difference in ULPs.
    int ulpsDiff = abs(a.i - b.i);

    return ulpsDiff <= maxUlpsDiff;
}

bool MathUtilities::EqualRelative(float n1, float n2, float maxDiff, float maxRelDiff) {
    float diff = fabs(n1 - n2);

    if (diff <= maxDiff)
        return true;

    n1 = fabs(n1);
    n2 = fabs(n2);

    float largest = (n2 > n1) ? n2 : n1;

    return diff <= (largest * maxRelDiff);
}

// Code from: http://stackoverflow.com/questions/19665818/best-way-to-generate-random-numbers-using-c11-random-library
unsigned MathUtilities::Random(unsigned min, unsigned max) {
    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<unsigned> dist(min, max);

    return dist(mt);
}
