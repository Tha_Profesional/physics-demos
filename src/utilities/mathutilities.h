/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef MATHUTILITIES_H_INCLUDED
#define MATHUTILITIES_H_INCLUDED

#include <float.h>

namespace MathUtilities {
    bool Within(float nt, float n1, float n2);

    bool Equal(float n1, float n2);
    bool EqualUlps(float n1, float n2, float maxDiff, int maxUlpsDiff);
    bool EqualRelative(float n1, float n2, float maxDiff, float maxRelDiff = FLT_EPSILON);

    unsigned Random(unsigned min, unsigned max);
}

#endif // MATHUTILITIES_H_INCLUDED
