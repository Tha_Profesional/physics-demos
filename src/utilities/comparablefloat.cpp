/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "comparablefloat.h"

using namespace std;

ComparableFloat::ComparableFloat(float n) : f(n) {}

bool ComparableFloat::Negative() const {
    return i < 0;
}

int32_t ComparableFloat::RawMantissa() const {
    return i & ((1 << 23) - 1);
}

int32_t ComparableFloat::RawExponent() const {
    return (i >> 23) & 0xFF;
}
