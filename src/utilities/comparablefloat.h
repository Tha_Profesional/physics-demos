/*
 * Special thanks to:
 *  - This excellent article:
 *      https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 *
 * Notes:
 * 	- None
 */

#ifndef COMPARABLEFLOAT_H_INCLUDED
#define COMPARABLEFLOAT_H_INCLUDED

#include <cstdint>

union ComparableFloat
{
    int32_t i;
    float f;

    ComparableFloat(float n = 0.0f);

    bool Negative() const;

    int32_t RawMantissa() const;
    int32_t RawExponent() const;
};

#endif // COMPARABLEFLOAT_H_INCLUDED
