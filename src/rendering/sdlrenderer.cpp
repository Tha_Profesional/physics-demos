/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "sdlrenderer.h"

using namespace std;

SDLRenderer::SDLRenderer(Window* w) : window(w) {}

void SDLRenderer::Render(SDL_Texture* t, Vector p, const SDL_Rect& c, float a) {
    SDL_Rect d;

    d.x = p.X;
    d.y = p.Y;

    if (SDL_RectEmpty(&c)) {
        SDL_QueryTexture(t, nullptr, nullptr, &d.w, &d.h);
    } else {
        d.w = c.w;
        d.h = c.h;
    }

    Render(t, d, c, a);
}

void SDLRenderer::Render(SDL_Texture* t, SDL_Rect& d, const SDL_Rect& c, float a) {
    if (t != nullptr) {
        if (a == 0.0) {
            if (SDL_RectEmpty(&c))
                SDL_RenderCopy(window->GetRenderer(), t, nullptr, &d);
            else
                SDL_RenderCopy(window->GetRenderer(), t, &c, &d);
        } else {
            SDL_Point center = {(d.w / 2), (d.h / 2)};

            if (SDL_RectEmpty(&c))
                SDL_RenderCopyEx(window->GetRenderer(), t, nullptr, &d, a, &center, SDL_FLIP_NONE);
            else
                SDL_RenderCopyEx(window->GetRenderer(), t, &c, &d, a, &center, SDL_FLIP_NONE);
        }
    }
}
