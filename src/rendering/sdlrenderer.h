/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef SDLRENDERER_H_INCLUDED
#define SDLRENDERER_H_INCLUDED

#include "SDL2/SDL.h"

#include "window.h"
#include "vector.h"

class SDLRenderer {
private:
    Window* window;
public:
    SDLRenderer(Window* w);

    void Render(SDL_Texture* t, Vector p, const SDL_Rect& c = {0, 0, 0, 0}, float a = 0.0);
    void Render(SDL_Texture* t, SDL_Rect& d, const SDL_Rect& c = {0, 0, 0, 0}, float a = 0.0);
};

#endif // SDLRENDERER_H_INCLUDED


