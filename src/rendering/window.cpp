/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 *  - None
 */

#include "window.h"

#include <cstring>

#include "SDL2/SDL_image.h"
#include "SDL2/SDL_mixer.h"

#include "globals.h"

using namespace std;

Window::Window() : window(nullptr, SDL_DestroyWindow), renderer(nullptr, SDL_DestroyRenderer) {}

Window::~Window() {
    Mix_CloseAudio();
    Mix_Quit();

    IMG_Quit();

    SDL_Quit();
}

bool Window::initSDL() {
    return (SDL_Init(SDL_INIT_EVERYTHING) == 0);
}

bool Window::initSDLImage() {
    return ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) == IMG_INIT_PNG);
}

bool Window::createWindow() {
    window.reset(SDL_CreateWindow("", 100, 100, Globals::SCREEN_WIDTH, Globals::SCREEN_HEIGHT, SDL_WINDOW_HIDDEN));

    return window.get() != nullptr;
}

bool Window::createRenderer() {
    renderer.reset(SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));

    if (renderer.get() != nullptr) {
        SDL_SetRenderDrawColor(renderer.get(), 255, 255, 255, 255);
        SDL_ShowWindow(window.get());
        Render();

        return true;
    }

    return false;
}

bool Window::Init() {
    return initSDL() && initSDLImage() && createWindow() && createRenderer();
}

void Window::Render() {
    SDL_RenderPresent(renderer.get());
}

void Window::ClearRenderer() {
    SDL_RenderClear(renderer.get());
}

SDL_Renderer* Window::GetRenderer() {
    return renderer.get();
}
