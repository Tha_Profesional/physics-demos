/*
 * Special thanks to:
 *      - No-one yet
 *
 * Notes:
 * 		- None
 */

#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <memory>

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

class Window {
private:
    std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> window;
    std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer*)> renderer;

    bool initSDL();

    bool initSDLImage();

    bool createWindow();
    bool createRenderer();
public:
    Window();
    ~Window();

    bool Init();

    void Render();
    void ClearRenderer();

    SDL_Renderer* GetRenderer();
};

#endif // WINDOW_H_INCLUDED
