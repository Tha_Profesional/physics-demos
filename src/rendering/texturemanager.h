/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef TEXTUREMANAGER_H_INCLUDED
#define TEXTUREMANAGER_H_INCLUDED

#include <vector>
#include <unordered_map>
#include <string>

#include "SDL2/SDL.h"

#include "window.h"
#include "textureid.h"

class TextureManager {
private:
    Window* window;

    std::vector<SDL_Texture*> textures;

    std::unordered_map<std::string, unsigned> map;
    std::unordered_map<unsigned, std::string> inverseMap;
public:
    TextureManager(Window* w);
    ~TextureManager();

    TextureId Create(std::string f);
    void Destroy(TextureId i);

    TextureId GetId(std::string f);
    SDL_Texture* GetTexture(TextureId i);
};

#endif // TEXTUREMANAGER_H_INCLUDED
