/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#include "texturemanager.h"

#include "SDL2/SDL_image.h"

using namespace std;
TextureManager::TextureManager(Window* w) : window(w) {}

TextureManager::~TextureManager() {
    for(vector<SDL_Texture*>::iterator it = textures.begin(); it != textures.end(); ++it)
        SDL_DestroyTexture(*it);
}

TextureId TextureManager::Create(string f) {
    if (map.find(f) == map.end()) {
        SDL_Texture* t = IMG_LoadTexture(window->GetRenderer(), f.c_str());

        if (t != nullptr) {
            textures.push_back(t);

            int i = textures.size() - 1;

            if (textures[i] != nullptr) {
                map[f] = i;
                inverseMap[i] = f;

                return { i };
            } else {
                textures.erase(textures.begin() + i);
            }
        }
    }

    return { -1 };
}

void TextureManager::Destroy(TextureId i) {
    int lastIndex = textures.size() - 1;

    textures[i.Id] = textures[lastIndex];

    textures.erase(textures.begin() + lastIndex);

    map[inverseMap[lastIndex]] = i.Id;
    map.erase(map.find(inverseMap[i.Id]));

    inverseMap[i.Id] = inverseMap[lastIndex];
    inverseMap.erase(inverseMap.find(lastIndex));
}

TextureId TextureManager::GetId(string f) {
    if (map.find(f) == map.end())
        return { -1 };
    else
        return { map[f] };
}

SDL_Texture* TextureManager::GetTexture(TextureId i) {
    return textures[i.Id];
}
