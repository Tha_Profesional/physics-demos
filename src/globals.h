/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

#include <string>

namespace Globals {
    constexpr unsigned ENTITY_INDEX_BITS = 12;
    constexpr unsigned ENTITY_INDEX_MASK = (1 << ENTITY_INDEX_BITS) - 1;

    constexpr unsigned ENTITY_GENERATION_BITS = 4;
    constexpr unsigned ENTITY_GENERATION_MASK = (1 << ENTITY_GENERATION_BITS) - 1;

    constexpr unsigned MINIMUM_FREE_INDICES = 1024;

    constexpr unsigned SCREEN_WIDTH = 1174;
    constexpr unsigned SCREEN_HEIGHT = 660;

    constexpr float MOVEMENT_SPEED = 100;

    constexpr double PI = 3.14159265358979323846;
    constexpr float TWO_PI_RAD = 360 * PI / 180;

    const std::string INPUT_CONFIG_FILE = "config/bindings.conf";

    constexpr int MAX_COLLISION_RETRIES = 2;
}

#endif // GLOBALS_H_INCLUDED
