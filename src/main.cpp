/* 
 * Special thanks to:
 *  - No-one yet.
 *
 * Notes:
 *  - None yet.
 */

#include "circlevs.h"
#include "statemanager.h"
#include "timer.h"
#include "window.h"

using namespace std;

int main(int argc, char* args[])
{
    Window* window = new Window();

    if (window->Init()) {
        StateManager* stateManager = new StateManager();

        Timer* timer = new Timer();

        // Set up initial state
        stateManager->AddState(new CircleVs(stateManager, window, timer));

        SDL_Event e;

        timer->Start();
        unsigned ticks = timer->Ticks();

        while (stateManager->GetStateCount()) {
            while (SDL_PollEvent(&e)) {
                if (e.type == SDL_QUIT)
                    stateManager->RemoveAllStates();
                else
                    stateManager->Input(e);
            }

            stateManager->Input();

            // Get the 'update' time for the loop
            unsigned updateTicks =  timer->Ticks() - ticks;
            ticks = timer->Ticks();

            stateManager->Update(updateTicks);

            stateManager->Render();

            // limit frames
        }

        delete timer;
        delete stateManager;
        delete window;
    }

    return 0;
}
