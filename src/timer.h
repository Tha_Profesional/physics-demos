/*
 * Special thanks to:
 *  - No-one yet
 *
 * Notes:
 * 	- None
 */

#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED

class Timer {
private:
    bool started;
    bool paused;

    int startTicks;
    int pausedTicks;
public:
    Timer();

    void Start();
    void Stop();
    void Pause();
    void Unpause();

    int Restart();

    int Ticks() const;

    bool Started() const;
    bool Paused() const;
};


#endif // TIMER_H_INCLUDED
